import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, useEffect } from "react"

export default function Logout(){

// localStorage.clear();

		// Consume user context object and destructure it to access the user state and unsetUser function

	const {unSetUser, setUser} = useContext(UserContext);
		// Redirect back to login
	
	// Clear the localStorage
	unSetUser();

	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null,
			email: null,
			token: null
		});
	})

	return(

		<Navigate to="/login" />


		);
}