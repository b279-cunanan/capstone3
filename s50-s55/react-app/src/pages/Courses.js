import courseData from "../data/courses.js"
import CourseCard from "../components/CourseCard";
import { useEffect, useState } from "react"



export default function Courses(){
	// checks to see if mock data is captured
	console.log (courseData);
	console.log (courseData[0]);

	// State that will be used to store courses retreived from db

	const [allCourses, setAllCourses] = useState([])

	// Retrieves the courses from db upon initial render of the "Courses" Component

	useEffect(() => {
		fetch(`http://localhost:4000/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllCourses(data.map(course => {
				return (
					<CourseCard key={course._id} courseProp={course}/>
					)
			}))


		})
	}, [])

	
	return(
		<>
			<h1>Courses</h1>
			{/*Prop making and prop passing*/}
			{allCourses}
		</>
		)

}