import React from "react";
import { Box, FooterContainer, Row, Column, FooterLink, Heading } from "./FooterStyles";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <Box className="pt-5 pb-3">
        <Column className="col-12 col-md-6 col-lg-4 footer-text">
            <div as={Link} to={"/"} className="footer-name text-start text-lg-start" >zine.repo</div>
            <p className="footer-text d-flex justify-content-start text-start text-lg-start mt-3">A community-run online bookshop that features the works of Filipino zinesters.</p>
        </Column>
      
    
        <FooterContainer className="col-12 col-lg-8 px-xl-5">
        <Row className="px-xl-4 pb-5 ">
            <Column>
            <Heading>Support</Heading>
            <FooterLink href="#">About</FooterLink>
            <FooterLink href="#" className="">Blog</FooterLink>
            <FooterLink href="#">Donate</FooterLink>
            </Column>

            <Column>
            <Heading>Zines & Books</Heading>
            <FooterLink href="#">Fiction</FooterLink>
            <FooterLink href="#">Non-Ficion</FooterLink>
            <FooterLink href="#">Writing</FooterLink>
            <FooterLink href="#">Workshops</FooterLink>
            </Column>

            <Column>
            <Heading>Follow Us</Heading>
            <FooterLink href="#"><i className="fab fa-facebook-f"><span className="ms-2 socmed">Facebook</span></i></FooterLink>
            <FooterLink href="#"><i className="fab fa-instagram"><span className="ms-2 socmed">Instagram</span></i></FooterLink>
            <FooterLink href="#"><i className="fab fa-twitter"><span className="ms-2 socmed">Twitter</span></i></FooterLink>
            </Column>


            
        </Row>

        
        </FooterContainer>
    </Box>
  );
};

export default Footer;