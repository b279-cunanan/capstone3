// object destructuring
import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import Carousel from 'react-bootstrap/Carousel';
import "./components.css";


export default function Banner(){
	// console.log(bannerProps);

	// const {title, content, destination, label} = bannerProps;
 
	return(
		<Carousel variant="dark" id="carousel" className="mt-5">

          <Carousel.Item >

            <div className="carouselImageOvelaybox">
              
              <img
                className="d-block carousel-img"
                src="https://purveyr.com/wp-content/uploads/2020/11/Purveyr-Fake-Zine-107-uai-2064x1376.jpg"
                alt="First slide"
              />

              <div className="carouselOverlay">
                <span className="carouselOverlayText">
                  <a href="https://purveyr.com/2020/11/17/celene-sakurako-puts-the-spotlight-on-creative-subcultures-one-fake-zine-at-a-time/">

                FAKE Zine: Unmasking Subcultural Artistry
                  </a>

                </span>
              </div>

            </div>

          </Carousel.Item>

          <Carousel.Item >
          <div className="carouselImageOvelaybox">
            <img
              className="d-block carousel-img"
              src="https://purveyr.com/wp-content/uploads/2023/02/electromilk-purveyr-zaldine-alvaro-mcedit-2023-12.jpg"
              alt="Second slide"
            />
              <div className="carouselOverlay">
                <span className="carouselOverlayText">
                  <a href="https://www.scoutmag.ph/46311/electromilks-art-erotica-politika/">Decoding the Enigmatic Language of Electromilk's Quiet and Marble-Esque Visuals
                  </a>
                </span>
              </div>
          </div>
            
          </Carousel.Item>

          <Carousel.Item >
          <div className="carouselImageOvelaybox">
            <img
              className="d-block carousel-img"
              src="https://purveyr.com/wp-content/uploads/2020/11/Purveyr-Fake-Zine-51-uai-1440x960.jpg"
              alt="Third slide"
            />
              <div className="carouselOverlay">
                <span className="carouselOverlayText">
                   <a href="https://purveyr.com/2020/11/17/celene-sakurako-puts-the-spotlight-on-creative-subcultures-one-fake-zine-at-a-time/">
                    Celene Sakurako's Platform for Creative Subculture Exploration
                    </a>
                </span>
              </div>
            
        </div>
          </Carousel.Item>

           <Carousel.Item >
           <div className="carouselImageOvelaybox">
            <img
              className="d-block carousel-img"
              src="https://purveyr.com/wp-content/uploads/2019/03/kwago-curated-shelf-purveyr-1.jpg"
              alt="Fourth slide"
            />
              <div className="carouselOverlay">
                <span className="carouselOverlayText" >
                   <a href="https://purveyr.com/2019/04/02/the-art-of-reading-people-as-told-by-kwago/">
                    Seeing the Unseen: Kwago's Art of Reading People
                    </a>
                </span>
              </div>
          </div>
            
          </Carousel.Item>
          <Carousel.Item >
          <div className="carouselImageOvelaybox">
            <img
              className="d-block carousel-img"
              src="https://purveyr.com/wp-content/uploads/2021/08/Purveyr-Marx-Fidel-68.jpg"
              alt="Fifth slide"
            />
              <div className="carouselOverlay">
                <span className="carouselOverlayText" >
                   <a href="https://purveyr.com/2021/08/06/marx-fidel-engages-the-political-monsters-through-art-and-artivism/">
                    Confronting Political Monsters: Amplifying Political Discourse through Artistic Expression
                    </a>
                </span>
            </div>
              </div>
            
          </Carousel.Item>
        </Carousel>
      );
    }

    

			

