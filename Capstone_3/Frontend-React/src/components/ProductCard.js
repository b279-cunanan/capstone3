import { Card, Button, OverlayTrigger, Tooltip} from "react-bootstrap";
// In React.js we have 3 hooks

/*
1. useState
2. useEffect
3. useContext
*/

import {useState} from "react";
import { Link } from "react-router-dom"
import "./components.css";
import styled from "@emotion/styled/macro";


export default function ProductCard({productProp}){
  // checks if props was successfully passed
  // console.log(productProp);
  // checks the type of the passed data
  // console.log(typeof productProp);

// Destructuring the courseProp into their own variables
  const { _id, name, description, price, image, category, rating } = productProp


  // console.log({name})

 //  // useState
 //  // Used for storing different states
 //  // used to keep track of information to individual components

 //  // SYNTAX -> const [getter, setter] = useState (initialGetterValue);

 //  const [count, setCount] = useState(0);
 //  console.log (useState(0));

 //  const [seat, setSeat] = useState(30);

 //  // Function that keeps track of the enrollees for a course
 // function enroll(){
 //     if(seat > 0){
 //         setCount(count + 1);
 //         setSeat(seat - 1);
 //     }else{
 //         alert("No more seats available!");
 //     }
     
 //     console.log("Enrollees: " + count);
 //     console.log("Available Seats: " + seat);
 // }

const DisplayOver = styled.div({
  height: "100%",
  left: "0",
  position: "absolute",
  top: "0",
  width: "100%",
  zIndex: 2,
  transition: "background-color 350ms ease",
  backgroundColor: "transparent",
  padding: "20px 20px 0 20px",
  boxSizing: "border-box",
});
  const Hover = styled.div({
  opacity: 0,
  transition: "opacity 350ms ease",
});

const SubTitle = styled.h4({
  
  transform: "translate3d(0,50px,0)",
  transition: "transform 350ms ease",
});

const Paragraph = styled.p({

  transform: "translate3d(0,50px,0)",
  transition: "transform 350ms ease",
});


const Background = styled.div({
    position: "relative",

  // Other background code
  [`:hover ${DisplayOver}`]: {
    backgroundColor: "rgba(0,0,0,.5)",
  },
  [`:hover ${SubTitle}, :hover ${Paragraph}`]: {
    transform: "translate3d(0,0,0)",
  },
  [`:hover ${Hover}`]: {
    opacity: 1,
  },
});







  return(
    // <>
    //     <Card className="my-3" id="product-cards">
    //       <Card.Body className="d-flex flex-column justify-content-between">
    //         <Card.Title>{name}</Card.Title>
    //         <Card.Subtitle>Description</Card.Subtitle>
    //         <Card.Text>{description}</Card.Text>
    //         <Card.Subtitle>Price:</Card.Subtitle>
    //         <Card.Text>{price}</Card.Text>
    //         <Card.Text id="category-text">{category}</Card.Text>
    //         <Card.Text>Rating: {rating}</Card.Text>
    //         <Link className="btn btn-primary" to={`/productView/${_id}`}>Details</Link>
    //       </Card.Body>
    //     </Card>

            <Card className="my-5 mx-3 d-flex flex-column align-items-center justify-content-center" id="product-cards">

              <Background>

              <Card.Img variant="top" src={image} id="image-product" className="card-image"/>

                <DisplayOver>
                

                  <Hover>
                    <Card.Body className="d-flex flex-column justify-content-space-between">

                      <SubTitle className="text-center mt-2 mb-4" id="product-title">{name}</SubTitle>

                      {/*<Card.Body className="d-flex flex-column justify-content-between">
                      <Card.Title>{name}</Card.Title>
                      <Card.Text>{description}</Card.Text>
                      <Card.Text>Price: {price}</Card.Text>
                      <Card.Text id="category-text">{category}</Card.Text>
                      <Card.Text>Rating: {rating}</Card.Text>
                      <Link className="btn btn-primary" to={`/productView/${_id}`}>Details</Link>
                      </Card.Body>*/}



                      {/*<Paragraph>{description}</Paragraph>*/}
                      <div>
                      <Paragraph className="text-center my-0 prod-deet">
                        Price: {price}
                      </Paragraph>
                      

                      <Paragraph className="text-center my-1 prod-deet d-flex align-items-center justify-content-center" id="category-text">
                        <div className="w-auto " id="category-box">
                          {category}
                        </div>
                      </Paragraph>
                      
                      <Paragraph className="text-center my-0 mb-auto prod-deet">
                        Rating: {rating}
                      </Paragraph>

                      </div>

                      <Paragraph className="text-center">
                        <Link className="btn btn-none" id="details-btn" to={`/productView/${_id}`}>
                          Details
                        </Link>
                      </Paragraph>

                    </Card.Body>
                  </Hover>
                </DisplayOver>
              </Background>
            </Card>

          
    )
}