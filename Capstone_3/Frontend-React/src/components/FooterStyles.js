// FooterStyles.js
import styled from 'styled-components';

export const Box = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: start;
    background-color: rgb(17, 17, 17);
    color: white;
    font-family: 'Inter', sans-serif;
    font-weight: 600;

    @media (min-width: 992px){
        display: flex;
        flex-direction: row;
        justify-content: start;
        align-items: center;
    }
`;

export const FooterContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: start;

    @media (min-width: 992px){
        align-items: end;
    }

`;

export const Row = styled.div`

    display: flex;
    flex-direction: column;
    justify-content: center;
    width: max-content;

    @media (min-width: 992px){
        display: flex;
        flex-direction: row;
        justify-content: between;
        width: max-content;
    }
`;

export const Column = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1rem 4rem;

    @media (min-width: 992px){
        padding: 0 4rem;
    }
`;

export const FooterLink = styled.a`
    color: white;
    text-decoration: none;
    margin: 2px 0;
    font-weight: 300;
    font-size: 14px;
`;

export const Heading = styled.h3`
    margin: 1rem 0;
    font-weight: 600;
    font-size: 18px;
`;
