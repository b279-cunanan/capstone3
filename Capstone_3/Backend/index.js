const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");
// Allows access to routes defined within our application
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");
const cartRoutes = require("./routes/cart");
// const categoryRoutes = require("./routes/category");
// const searchRoutes = require("./routes/search");
// const reviewRoutes = require("./routes/review");






require('dotenv').config();
// const db = require('./db.js');

// Import the db.js file and invoke the run() function
// db.run().catch(console.dir);

// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
const app = express();

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.tr9npll.mongodb.net/Capstone_3?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));

// **************************************************

// Allows all resources to access our backend application
// app.use(cors({
//   origin: 'https://capstone3-fe.vercel.app/'
// }));



// const corsOptions = {
//   origin: '*',
// };




// const corsOptions = {
//   origin: '*',
// };


// // Set the allowed origins based on your frontend application's domain
// const allowedOrigins = ['https://capstone3-fe.vercel.app'];

// // CORS middleware configuration
// const corsOptions = {
//   origin: (origin, callback) => {
//     // Check if the origin is in the allowed origins array
//     if (allowedOrigins.includes(origin)) {
//       callback(null, true);
//     } else {
//       callback(new Error('Not allowed by CORS'));
//     }
//   },
// };

// // Enable CORS middleware
// app.use(cors(corsOptions));


// **************************************************
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'https://capstone3-fe.vercel.app');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/user", userRoutes);
// Defines the "/courses" string to be included for all course routes defined in the "course" route file
app.use("/product", productRoutes);
app.use("/order", orderRoutes);
app.use("/cart", cartRoutes);
// app.use("/category", categoryRoutes);
// app.use("/search", searchRoutes);
// app.use("/review", reviewRoutes);



// Will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});