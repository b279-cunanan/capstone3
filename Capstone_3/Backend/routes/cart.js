const express = require('express');
const router = express.Router();
const auth = require("../auth.js");

// Import the Cart controller
const cartController = require('../controllers/cartController');

// #############################################################################

// POST /cart - Add a product to the cart
router.post('/:productId', auth.verify,(req, res) => {
  let userAuth = auth.decode(req.headers.authorization);
  cartController.addToCart(req.params.productId, userAuth)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});
 
// #############################################################################

// GET /cart - Retrieve the user's cart
router.get('/', auth.verify,(req, res) => {
  let userAuth = auth.decode(req.headers.authorization);
  cartController.getCartItems(userAuth)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

// #############################################################################

// PUT /cart/:productId - Update the quantity of a product in the cart
router.put('/:productId', auth.verify, (req, res) => {
  let userAuth = auth.decode(req.headers.authorization);
  cartController.updateCartItem(req, req.params.productId, userAuth)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

// #############################################################################

// DELETE /:productId - Remove a product from the cart
router.delete('/:productId', auth.verify, (req, res) => {
  let userAuth = auth.decode(req.headers.authorization);
  cartController.removeFromCart(req.params.productId, userAuth)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

// #############################################################################

// Clear cart for a user
router.delete('/clear/:userId', auth.verify, (req, res) => {
   let userAuth = auth.decode(req.headers.authorization);
  cartController.clearCart(req.params.userId, res, userAuth) 
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});

// #############################################################################

// GET /cart - Retrieve the user's cart
router.post('/', auth.verify,(req, res) => {
  let userAuth = auth.decode(req.headers.authorization);
  cartController.getUserCart(userAuth)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send(error.message));
});


module.exports = router;

















































