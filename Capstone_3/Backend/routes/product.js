const express =require("express"); 
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js"); 



// Create PRODUCT
router.post("/create", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
	
});

// Get all products
router.get("/all", (req, res) => {
	
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Get all "ACTIVE" products
router.get("/active", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Retrieve specific product
router.get("/search/:productId", (req, res) => {
	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));

})

// Update a product
router.put("/update/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params.productId, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})


// Delete a specific product
router.delete('/delete/:productId', (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  productController.deleteProduct(req.params.productId, isAdmin).then(resultFromController => res.send(resultFromController));
});



// Archive a product
router.put("/archive/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params.productId, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Activate a product
router.put("/activate/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.activateProduct(req.params.productId, isAdmin).then(resultFromController => res.send(resultFromController));
})

// // Retrieve ALL ORDERS
// router.post("/orders", auth.verify, (req, res) => {
// 	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
// 	productController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));

// })



// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;


