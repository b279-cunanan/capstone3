const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true
  },
  products: [
    {
      productId: {
        type: String,
        required : [true, "PRODUCT ID is required!"] 
      },
      quantity: {
        type: Number,
        default: 1,
        min: 1
      },
      subtotal: {
        type: Number,
        default: 0
      }
    }
  ],
  total: {
      type: Number,
      default: 0
  }
});



module.exports = mongoose.model('Cart', cartSchema);
