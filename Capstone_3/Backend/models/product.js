const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Course is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	category: {
	    type: String,
	    required: [true, "Price is required"]
  	},
  	image: {
	    type: String,
	    required: [true, "image link is required"]
  	},
  	rating: {
  	    type: Number,
  	    default: 0
  	},
  	reviews: [
  				{
			  	    user: {
			  	      type: String,
			  	      required: true
			  	    },
			  	    rating: {
			  	      type: Number,
			  	      required: true
			  	    },
			  	    comment: {
			  	      type: String,
			  	      required: true
			  	    }
  				}
  	],
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		
		default : new Date()
	},
	userOrders : [
		{
			userId : {
				type : String,
				required: [true, "UserId is required"]
			},
			purchasedOn : {
				type : Date,
				default : new Date() 
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);