const Order = require('../models/order');
const Cart = require('../models/cart');
const User = require('../models/user');
const Product = require('../models/product');

// ############################################################################# 

// Create a new order
exports.checkoutOrder = async (req, userAuth) => {
  const  userIdNew  = userAuth.id;
  const  userAddress = req.shippingAddress;
  const  userContact = req.contactNumber;

  if (!userAuth.isAdmin) {
    try {
      // Find the user's cart and populate the product details
      const cart = await Cart.findOne({userId: userIdNew})
      console.log `This is the USER CART ${cart}`

      if (!cart || cart.products.length === 0) {
        return false;
      }

      // Create a new order based on the cart items

      const userInfo = await User.findById(userIdNew)

      console.log `This is the USER INFO ${userInfo}`

      let orderProduct = cart.products.map((cartItem) => ({
          productId: cartItem.productId,
          quantity: cartItem.quantity,
          subtotal: cartItem.subtotal
          }))

      console.log `This is the ORDER PRODUCT ${orderProduct}`


      // order.products.push(...orderProduct) 

      let order = new Order( {
        
        firstName: userInfo.firstName,
        lastName: userInfo.lastName,
        email: userInfo.email, 
        products: orderProduct, 
        total: cart.total,
        shippingAddress: userAddress,
        contactNumber: userContact

      } );

      console.log `This is the NEW ORDER ${order}`


      // Save the order
      await order.save();

      // Clear the user's cart after creating the order
      await Cart.findOneAndDelete( userIdNew );

      // UPDATE USER

      let newUserOrder = {
         
          orderId: order._id, 
          products: order.products,
          total: order.total,
          shippingAddress: order.shippingAddress,
          contactNumber:order.contactNumber,
          orderDate: order.orderDate
      }


      console.log `This is the TEMP ORDER in USER PROFILE ${newUserOrder}`





      userInfo.orders.push(newUserOrder)
      await userInfo.save(); 

      console.log `This is the NEW USER with ORDER ${userInfo}` 

      // UPDATE PRODUCTS

      let userOrderProd = {
          userId : userIdNew
      } 
      
      

      for (let i = 0; i < order.products.length; i++) {
            let productTemp = order.products[i];
           productInfo = await Product.findById(order.products[i].productId);
             console.log `This is the PRODUCT Info ${productInfo}` 


            productInfo.userOrders.push(userOrderProd) 

            console.log `This is the NEW PRODUCT with ORDER ${productInfo.userOrders}` 

            await productInfo.save();

      }

      







      return order;
    } catch (error) {
      return ({ error: 'An error occurred while creating the order.' });
    }

  }

  let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
      return value
    })



};






// #############################################################################

// Get all orders
exports.getAllOrders = async (req, res, userAuth) => {
  
if (userAuth.isAdmin) {
  try {
    const orders = await Order.find();
    res.json(orders);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching orders.' });
  }

}

  let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
      return value
    })
};

// #############################################################################

// Get a single order by ID
exports.getOrderById = async (req, res, userAuth) => {
  const orderId = req.params.id;
  

  try {
    const order = await Order.findById(orderId);
    if (!order) {
      return res.status(404).json({ error: 'Order not found.' });
    }
    res.json(order);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching the order.' });
  }

};

// #############################################################################

// Update an order
exports.updateOrder = async (req, res, userAuth) => {
  const orderId = req.params.id;
  const { userId, productId } = req.body;

  
if (!userAuth.isAdmin) {
  try {
    // Find the user's cart or create a new one if it doesn't exist
    let cart = await Cart.findOne({ userId });
    if (!cart) {
      cart = new Cart({ userId, products: [] });
    }

    // Check if the product is already in the cart
    const existingProduct = cart.products.find(
      (product) => product.productId.toString() === productId
    );

    if (existingProduct) {
      // If the product is already in the cart, increment its quantity
      existingProduct.quantity++;
    } else {
      // If the product is not in the cart, add it as a new cart item
      cart.products.push({ productId, quantity: 1 });
    }

    // Save the updated cart
    await cart.save();

    // Find and update the order
    const order = await Order.findByIdAndUpdate(
      orderId,
      { $push: { products: { productId, quantity: 1 } } },
      { new: true }
    );

    if (!order) {
      return res.status(404).json({ error: 'Order not found.' });
    }

    res.json(order);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while updating the order.' });
  }

}

  let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
      return value
    })
};

// #############################################################################

// Delete an order
exports.deleteOrder = async (req, res, userAuth) => {
  const orderId = req.params.id;
  
if (!userAuth.isAdmin) {
  try {
    const deletedOrder = await Order.findByIdAndDelete(orderId);
    if (!deletedOrder) {
      return res.status(404).json({ error: 'Order not found.' });
    }
    res.json({ message: 'Order deleted successfully.' });
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while deleting the order.' });
  }

}

  let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
      return value
    })
};
