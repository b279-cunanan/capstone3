const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/product") 





module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if (result.length > 0) {

			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;

		};
	});

};

// #############################################################################

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName, 
		email: reqBody.email,
		// 10 is the value provided as the number of salt rounds
		password: bcrypt.hashSync(reqBody.password, 10) 
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else {
			return true;
		}
	})
};

// #############################################################################

// Function to login a user
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		console.log(result)
		if (result == null) {
			return false
		}else {
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

				if(isPasswordCorrect) {
					return {access: auth.createAccessToken(result)}
				}else {
					return false
				}
			  }
	})

};

// #############################################################################

// Get ALL user info for User ID
module.exports.getAllUser = () => {
	return User.find({}).then(result => {
		return result;
	})
}

// #############################################################################

// Retrieve details of a user
module.exports.detailsUser = (data) => {
	return User.findById(data).then(result => {
		if (result == null) {
			return false
		} else {

			result.password = ""
			
			return result
				} 
	})
}

// #############################################################################

// RETRIEVE USER ORDER DETAILS
module.exports.orderDetailsUser = (data) => {
	return User.findById(data).then(result => {
		if (result == null) {
			return false
		} else {
			
			return result.orders
				} 
	})
}

// #############################################################################
	


// // Product checkout of a non-admin User 
// module.exports.checkout = async (data, userIdAuth, isAdmin) => {



// 	if(isAdmin == false){

// 		let productTemp = [];
// 		let totalAmountTemp = 0;
// 		let isProductUpdated;


// // Temp storage of each products

// 		data.products.forEach(singleProduct => {

// 		    console.log(singleProduct);
// 		    Product.findById(singleProduct.productId).then(product => {
// 		        productTemp.push(
// 		            {
// 		                productId : singleProduct.productId,
// 		                productName : product.name,
// 		                quantity : singleProduct.quantity
// 		            }
// 		        )
		        
// 		        totalAmountTemp = totalAmountTemp + (product.price * singleProduct.quantity)
// 		        console.log (totalAmountTemp)
// 		    })


// // Operations on Poduct and USER
		    

// 		isProductUpdated =  Product.findById(singleProduct.productId).then(product =>{

// 				// console.log(product);
// 		        product.userOrders.push({userId : userIdAuth,
// 		    							 orderId : data.products.productId});

// 		        return product.save().then((product, error) => {
// 		            if(error){
// 		                return false;
// 		            } else {
// 		                return true;
// 		            }
// 		        })
// 		    })
// 		});


// 		let isUserUpdated = await User.findById(userIdAuth).then(user => {
//                 user.orderedProduct.push(
//                     {

//                         products : productTemp,
//                         totalAmount : totalAmountTemp
//                     }
//                 );
//                 return user.save().then((user, error) => {
//                     if(error){
//                         return false;
//                     }else{
//                         return true;
//                     }
//                 })
//         })




// // RETURN MESSAGES

// 		if(isProductUpdated && isUserUpdated){
// 			return "Product checkout successful";
// 		}else{
// 			return "Something went wrong with your request. Please try again later";
// 		}

		

// 	}

// 	let message = Promise.resolve("You don't have the access rights to do this action.");

// 		return message.then((value) => {
// 			return value
// 		})
// }

// #############################################################################

// SET USER AS ADMIN
module.exports.updateUser = (reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedUser = {
		isAdmin : reqBody.isAdmin
		}

		return User.findByIdAndUpdate(reqBody.userId, updatedUser).then((user, error) => {
			if(error){
				return false;
			}else{
				return "User admin status updated!";
			}
		})
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

	
}

// #############################################################################

// Delete a user
exports.deleteUser = async (reqParams, userData) => {
  const userId = reqParams.userId;
  console.log(reqParams)
  try {
    const deletedUser = await User.findByIdAndDelete(userId);
    if (!deletedUser) {
      return res.status(404).json({ error: 'User not found.' });
    }
    return "User deleted successfully.";
  } catch (error) {
    return "An error occurred while deleting the user.";
  }
};












// *******************************START OF OTHER CODE****************************

// const User = require('../models/User');

// // Get all users
// exports.getAllUsers = async (req, res) => {
//   try {
//     const users = await User.find();
//     res.json(users);
//   } catch (error) {
//     res.status(500).json({ error: 'An error occurred while fetching users.' });
//   }
// };

// // Get a single user by ID
// exports.getUserById = async (req, res) => {
//   const userId = req.params.id;
//   try {
//     const user = await User.findById(userId);
//     if (!user) {
//       return res.status(404).json({ error: 'User not found.' });
//     }
//     res.json(user);
//   } catch (error) {
//     res.status(500).json({ error: 'An error occurred while fetching the user.' });
//   }
// };

// // Create a new user
// exports.createUser = async (req, res) => {
//   const { name, email, password } = req.body;
//   try {
//     const newUser = new User({ name, email, password });
//     const savedUser = await newUser.save();
//     res.status(201).json(savedUser);
//   } catch (error) {
//     res.status(500).json({ error: 'An error occurred while creating the user.' });
//   }
// };

// // Update a user
// exports.updateUser = async (req, res) => {
//   const userId = req.params.id;
//   const { name, email, password } = req.body;
//   try {
//     const updatedUser = await User.findByIdAndUpdate(
//       userId,
//       { name, email, password },
//       { new: true }
//     );
//     if (!updatedUser) {
//       return res.status(404).json({ error: 'User not found.' });
//     }
//     res.json(updatedUser);
//   } catch (error) {
//     res.status(500).json({ error: 'An error occurred while updating the user.' });
//   }
// };

// // Delete a user
// exports.deleteUser = async (req, res) => {
//   const userId = req.params.id;
//   try {
//     const deletedUser = await User.findByIdAndDelete(userId);
//     if (!deletedUser) {
//       return res.status(404).json({ error: 'User not found.' });
//     }
//     res.json({ message: 'User deleted successfully.' });
//   } catch (error) {
//     res.status(500).json({ error: 'An error occurred while deleting the user.' });
//   }
// };





